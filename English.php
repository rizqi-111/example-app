<?php
namespace Ariz\Dict;

class English{
    public static function translate($word)
    {
        switch ($word) {
            case 'satu':
                return "one";
                break;

            case 'dua':
                return "two";
                break;

            case 'tiga':
                return "three";
                break;

            default:
                return "I Dont Know";
                break;
        }
    }
}
?>
