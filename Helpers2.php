<?php

if(! function_exists('translateToEnglishLanguage')) {
    function translateToEnglishLanguage($word) {
        return \Ariz\Dict\English::translate($word);
    }
}
