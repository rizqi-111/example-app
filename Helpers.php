<?php

if(! function_exists('translateToSundaLanguage')) {
    function translateToSundaLanguage($word) {
        return \Ariz\Dict\Sunda::translate($word);
    }
}
